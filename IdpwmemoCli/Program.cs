﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace IdpwmemoCli
{
    static class Program
    {
        static void Main(string[] args)
        {
            string memoName = null;
            string serviceName = null;
            bool interactive = false;
            string targetValue = null;
            bool caseSensitive = false;
            if (!Program.ParseArgs(args, ref memoName, ref serviceName, ref interactive, ref targetValue, ref caseSensitive))
            {
                Console.Error.WriteLine("ERROR! Invalid option: {0}", String.Join(" ", args));
                Program.ShowUsage();
                Console.Error.WriteLine("Canceled");
                return;
            }
            if (interactive)
            {
                Program.Interactive();
                return;
            }
            string memoPath = null;
            if (!Program.ResolveMemoPath(ref memoName, ref memoPath))
            {
                if (memoName != null)
                {
                    Console.Error.WriteLine("ERROR! Not found memo {0}", memoName);
                }
                Console.Error.WriteLine("Canceled");
                return;
            }
            Console.WriteLine("Select memo: {0}", memoName);
            byte[] buf = File.ReadAllBytes(memoPath);
            Idpwmemo.IDPWMemo memo = Program.LoadMemo(buf);
            if (memo == null)
            {
                Console.Error.WriteLine("Canceled");
                return;
            }
            if (targetValue != null)
            {
                Program.FindValue(memo, targetValue, caseSensitive);
                return;
            }
            if (!Program.ResolveService(memo, serviceName))
            {
                Console.Error.WriteLine("Canceled");
                return;
            }
            int width = 0;
            foreach (Idpwmemo.Value v in memo.GetValues())
            {
                width = Math.Max(width, v.TypeName.Length);
            }
            foreach (Idpwmemo.Value v in memo.GetSecrets())
            {
                width = Math.Max(width, v.TypeName.Length);
            }
            Console.WriteLine("DETAILS ({0})", memo.GetValues().Count);
            foreach (Idpwmemo.Value v in memo.GetValues())
            {
                Console.WriteLine(" {0}: {1}", v.TypeName.PadLeft(width), v.Data);
            }
            Console.WriteLine("SECRETS ({0})", memo.GetSecrets().Count);
            foreach (Idpwmemo.Value v in memo.GetSecrets())
            {
                Console.WriteLine(" {0}: {1}", v.TypeName.PadLeft(width), v.Data);
            }
            memo.Clear();
            memo = null;
        }

        private static void FindValue(Idpwmemo.IDPWMemo memo, string targetValue, bool caseSensitive)
        {
            if (!caseSensitive)
            {
                targetValue = targetValue.ToLower();
            }
            var foundAny = false;
            for (int i = 0; i < memo.ServiceCount; i++)
            {
                memo.SelectService(i);
                string serviceName = memo.GetSelectedServiceName();
                var found = false;
                foreach (Idpwmemo.Value v in memo.GetValues())
                {
                    if (caseSensitive)
                    {
                        if (!v.Data.Contains(targetValue))
                        {
                            continue;
                        }
                    }
                    else if (!v.Data.ToLower().Contains(targetValue))
                    {
                        continue;
                    }
                    if (!found)
                    {
                        found = true;
                        Console.WriteLine("FOUND in {0}", serviceName);
                    }
                    Console.WriteLine("  DETAILS: {0}: {1}", v.TypeName, v.Data);
                }
                foreach (Idpwmemo.Value v in memo.GetSecrets())
                {
                    if (caseSensitive)
                    {
                        if (!v.Data.Contains(targetValue))
                        {
                            continue;
                        }
                    }
                    else if (!v.Data.ToLower().Contains(targetValue))
                    {
                        continue;
                    }
                    if (!found)
                    {
                        found = true;
                        Console.WriteLine("FOUND in {0}", serviceName);
                    }
                    Console.WriteLine("  SECRETS: {0}: {1}", v.TypeName, v.Data);
                }
                if (found)
                {
                    foundAny = true;
                    Console.WriteLine();
                }
            }
            if (!foundAny)
            {
                Console.WriteLine("not found");
            }
        }

        private static bool ResolveService(Idpwmemo.IDPWMemo memo, string serviceName)
        {
            List<string> nameList = memo.GetServiceNames();
            if (nameList.Count == 0)
            {
                Console.Error.WriteLine("ERROR! Not found any service");
                return false;
            }
            if (!String.IsNullOrWhiteSpace(serviceName))
            {
                int index = Program.FindNameIndex(nameList, serviceName);
                if (index < 0)
                {
                    Console.Error.WriteLine("ERROR! Not found service {0}", serviceName);
                    return false;
                }
                memo.SelectService(index);
                return true;
            }
            int idWidth = Program.IntegerWidth(nameList.Count);
            Console.WriteLine("{0}:(CANCEL)", "0".PadLeft(idWidth));
            for (int i = 0; i < nameList.Count; i++)
            {
                string n = (i + 1).ToString().PadLeft(idWidth);
                Console.WriteLine("{0}: {1}", n, nameList[i]);
            }
            for (int i = 0; i < 10; i++)
            {
                Console.Write("service number ? ");
                Console.Out.Flush();
                string line = Console.ReadLine();
                int index = 0;
                if (Int32.TryParse(line, out index))
                {
                    if (index == 0)
                    {
                        return false;
                    }
                    if (index > 0 && index <= nameList.Count)
                    {
                        memo.SelectService(index - 1);
                        return true;
                    }
                }
                else
                {
                    index = Program.FindNameIndex(nameList, line);
                    if (index >= 0)
                    {
                        memo.SelectService(index);
                        return true;
                    }
                }
                Console.WriteLine("ERROR! Invalid number");
            }
            Console.Error.WriteLine("ERROR! Too many try");
            return false;
        }

        private static int FindNameIndex(List<string> list, string target)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] == target)
                {
                    return i;
                }
            }
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].StartsWith(target))
                {
                    return i;
                }
            }
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Contains(target))
                {
                    return i;
                }
            }
            return -1;
        }

        private static string ReadPassword()
        {
            Console.Write("Passwoard ? ");
            Console.Out.Flush();
            string password = "";
            try
            {
                for (; ; )
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);
                    if (cki.Key == ConsoleKey.Enter)
                    {
                        Console.WriteLine();
                        break;
                    }
                    if (cki.KeyChar == '\u0000')
                    {
                        continue;
                    }
                    if (Char.IsWhiteSpace(cki.KeyChar) || !Char.IsControl(cki.KeyChar))
                    {
                        password += cki.KeyChar;
                        Console.Write("*");
                        Console.Out.Flush();
                    }
                }
            }
            catch
            {
                password = Console.ReadLine();
            }
            return password;
        }

        private static Idpwmemo.IDPWMemo LoadMemo(byte[] buf)
        {
            var memo = new Idpwmemo.IDPWMemo();
            for (int i = 0; i < 10; i++)
            {
                string password = Program.ReadPassword();
                memo.SetPassword(password);
                if (memo.LoadMemo(buf))
                {
                    return memo;
                }
                Console.WriteLine("ERROR! Wrong password");
            }
            Console.Error.WriteLine("ERROR! Too many wrong password");
            return null;
        }

        private static bool ParseArgs(string[] args, ref string memoName, ref string serviceName, ref bool interactive, ref string targetValue, ref bool caseSensitive)
        {
            if (args == null || args.Length == 0)
            {
                return true;
            }
            if (args.Length == 1 && (args[0] == "-e" || args[0].ToLower() == "/e"))
            {
                interactive = true;
                return true;
            }
            if (args.Length % 2 != 0 || args.Length > 4)
            {
                return false;
            }
            if (args[0] == "-x" || args[0].ToLower() == "/x")
            {
                targetValue = args[1].Trim();
                caseSensitive = false;
                return args.Length == 2;
            }
            if (args[0] == "-xc" || args[0].ToLower() == "/xc")
            {
                targetValue = args[1].Trim();
                caseSensitive = true;
                return args.Length == 2;
            }
            if (args[0] != "-m" && args[0].ToLower() != "/m")
            {
                return false;
            }
            memoName = args[1].Trim();
            if (args.Length == 2)
            {
                return true;
            }
            if (args[2] == "-x" || args[2].ToLower() == "/x")
            {
                targetValue = args[3];
                caseSensitive = false;
                return true;
            }
            if (args[2] == "-xc" || args[2].ToLower() == "/xc")
            {
                targetValue = args[3];
                caseSensitive = true;
                return true;
            }
            if (args[2] != "-s" && args[2].ToLower() != "/s")
            {
                return false;
            }
            serviceName = args[3];
            return true;
        }

        private static void ShowUsage()
        {
            string programName = "";
            if ((Environment.GetCommandLineArgs()?.Length ?? 0) > 0)
            {
                programName = Environment.GetCommandLineArgs()[0];
            }
            if (String.IsNullOrWhiteSpace(programName))
            {
                programName = "IdpwmemoCli";
            }
            else
            {
                programName = Path.GetFileName(programName);
            }
            var dst = Console.Error;
            dst.WriteLine(@"
IDPWMemoCLI

USAGE:
  {0} [-m <MEMO_NAME> [-s <SERVICE_NAME>]]
  {0} [-m <MEMO_NAME>] -x <TARGET_VALUE>
  {0} [-m <MEMO_NAME>] -xc <TARGET_VALUE>
  {0} -e

OPTIONS
  -m <MEMO_NAME>      Memo name
  -s <SERVICE_NAME>   Service name
  -x                  Find Value in a Memo
  -xc                 Find Value in a Memo (case-sensitive)
  -e                  Edit mode
", programName);
        }

        private static string GetJavaMemoDirectory()
        {
            string dir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            dir = Path.Combine(dir, ".idpwmemo");
            return dir;
        }

        private static string GetWinMemoDirectory()
        {
            string dir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            dir = Path.Combine(dir, "IDPWMemo");
            return dir;
        }

        private static string GetMemoDirectory()
        {
            string dir = Program.GetJavaMemoDirectory();
            if (!Directory.Exists(dir))
            {
                dir = Program.GetWinMemoDirectory();
            }
            return dir;
        }

        private static List<Tuple<string, string>> GetMemoFiles(string dir)
        {
            string[] files = Directory.GetFiles(dir);
            var list = new List<Tuple<string, string>>();
            foreach (string f in files)
            {
                if (Path.GetExtension(f) != ".memo")
                {
                    continue;
                }
                string name = Path.GetFileNameWithoutExtension(f);
                list.Add(new Tuple<string, string>(f, name));
            }
            return list;
        }

        private static bool ResolveMemoPath(ref string memoName, ref string memoPath)
        {
            if (!String.IsNullOrWhiteSpace(memoName))
            {
                return Program.FindMemoPath(memoName, ref memoPath);
            }
            string dir = Program.GetMemoDirectory();
            if (!Directory.Exists(dir))
            {
                Console.Error.WriteLine("ERROR! Not found any memo");
                return false;
            }
            List<Tuple<string, string>> list = Program.GetMemoFiles(dir);
            if (list.Count == 0)
            {
                Console.Error.WriteLine("ERROR! Not found any memo");
                return false;
            }
            Console.WriteLine("{0}:(CANCEL)", "0".PadLeft(Program.IntegerWidth(list.Count - 1)));
            Program.ShowMemoList(list);
            for (int i = 0; i < 10; i++)
            {
                Console.Write("memo number ? ");
                Console.Out.Flush();
                string line = Console.ReadLine();
                int index = 0;
                if (Int32.TryParse(line, out index))
                {
                    if (index == 0)
                    {
                        return false;
                    }
                    if (index > 0 && index <= list.Count)
                    {
                        memoPath = list[index - 1].Item1;
                        memoName = list[index - 1].Item2;
                        return true;
                    }
                }
                else
                {
                    foreach (Tuple<string, string> elem in list)
                    {
                        if (elem.Item2 == line)
                        {
                            memoPath = elem.Item1;
                            memoName = elem.Item2;
                            return true;
                        }
                    }
                }
                Console.WriteLine("ERROR! Invalid number");
            }
            Console.Error.WriteLine("ERROR! Too many try");
            return false;
        }

        private static int IntegerWidth(int n)
        {
            n = Math.Max(1, n);
            return unchecked((int)Math.Floor(Math.Log10((double)n) + 1.0));
        }

        private static void ShowMemoList(List<Tuple<string, string>> list)
        {
            int windowWidth = Program.GetWindowWidth();
            int idWidth = Program.IntegerWidth(list.Count);
            int colCount = list.Count;
            int[] colSizes;
            int rowCount;
            do
            {
                rowCount = (list.Count + colCount - 1) / colCount;
                colSizes = new int[colCount];
                for (int i = 0; i < list.Count; i++)
                {
                    int p = i / rowCount;
                    colSizes[p] = Math.Max(colSizes[p], list[i].Item2.Length);
                }
                int width = colCount * (idWidth + 4) - 2;
                foreach (int s in colSizes)
                {
                    width += s;
                }
                if (width <= windowWidth)
                {
                    break;
                }
                colCount -= 1;
            } while (colCount > 1);
            for (int r = 0; r < rowCount; r++)
            {
                for (int c = 0; c < colCount; c++)
                {
                    int i = r + c * rowCount;
                    if (i >= list.Count)
                    {
                        break;
                    }
                    Console.Write((i + 1).ToString().PadLeft(idWidth));
                    Console.Write(": ");
                    Console.Write(list[i].Item2.PadRight(colSizes[c]));
                    Console.Write("  ");
                }
                Console.WriteLine();
            }
        }

        private static int GetWindowWidth()
        {
            try
            {
                return Console.WindowWidth;
            }
            catch
            {
                return 80;
            }
        }

        private static Regex GetMemoNameRegex()
        {
            return new Regex(@"^[_a-zA-Z0-9\-\(\)\[\]]{1,50}$");
        }

        private static bool FindMemoPath(string memoName, ref string memoPath)
        {
            Regex regex = Program.GetMemoNameRegex();
            if (String.IsNullOrWhiteSpace(memoName) || !regex.IsMatch(memoName))
            {
                return false;
            }
            // Java IDPWMemo Dir
            string originalDir = Program.GetJavaMemoDirectory();
            // IDPWMemoWin Dir
            string winDir = Program.GetWinMemoDirectory();

            string[] pathList = {
                Path.Combine(originalDir, memoName + ".memo"),
                Path.Combine(originalDir, memoName),
                Path.Combine(winDir, memoName + ".memo"),
                Path.Combine(winDir, memoName),
                memoName + ".memo",
                memoName
            };
            foreach (string path in pathList)
            {
                var fileInfo = new FileInfo(path);
                if (fileInfo.Exists)
                {
                    memoPath = fileInfo.FullName;
                    return true;
                }
            }
            return false;
        }

        private static void Interactive()
        {
            Console.WriteLine("IDPWMemoCLI - Edit mode -");
            Console.WriteLine();
            string dir = Program.GetMemoDirectory();
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            string memoName = Program.InteractiveSelectMemo(dir);
            if (memoName == null)
            {
                return;
            }
            var memo = new Idpwmemo.IDPWMemo();
            string memoPath = Path.Combine(dir, memoName + ".memo");
            string password = Program.ReadPassword();
            memo.SetPassword(password);
            if (File.Exists(memoPath))
            {
                byte[] buf = File.ReadAllBytes(memoPath);
                int limit = 10;
                while (!memo.LoadMemo(buf))
                {
                    Console.WriteLine("ERROR! Wrong password");
                    limit--;
                    if (limit <= 0)
                    {
                        Console.Error.WriteLine("ERROR! Too many wrong password");
                        return;
                    }
                    password = Program.ReadPassword();
                }
            }
            else
            {
                memo.NewMemo();
            }
            password = null;

            for (; ; )
            {
                Console.WriteLine();
                Console.WriteLine("[ {0} ]", memoName);
                Console.WriteLine("0:(QUIT)");
                Console.WriteLine("1: Change password");
                Console.WriteLine("2: Add new service");
                if (memo.ServiceCount > 0)
                {
                    Console.WriteLine("3: Edit service");
                }
                bool ok = false;
                for (int i = 0; i < 10; i++)
                {
                    Console.Write("command number ? ");
                    Console.Out.Flush();
                    string line = Console.ReadLine().Trim().ToLower();
                    int index = 0;
                    if (Int32.TryParse(line, out index))
                    {
                        switch (index)
                        {
                            case 0: // Quit
                                return;
                            case 1: // Change password
                                ok = true;
                                Program.InteractiveChangePassword(memoPath, memo);
                                break;
                            case 2: // Add new service
                                ok = true;
                                Program.InteractiveAddNewService(memoName, memoPath, memo);
                                break;
                            case 3: // Edit service
                                if (memo.ServiceCount > 0)
                                {
                                    ok = true;
                                    Program.InteractiveEditService(memoName, memoPath, memo);
                                }
                                break;
                            default:
                                break;
                        }
                        if (ok)
                        {
                            break;
                        }
                    }
                    else if ("quit".StartsWith(line))
                    {
                        return;
                    }
                    else if ("change password".StartsWith(line) || "change password".EndsWith(line))
                    {
                        ok = true;
                        Program.InteractiveChangePassword(memoPath, memo);
                        break;
                    }
                    else if ("add new service".StartsWith(line))
                    {
                        ok = true;
                        Program.InteractiveAddNewService(memoName, memoPath, memo);
                        break;
                    }
                    else if ("edit service".StartsWith(line))
                    {
                        if (memo.ServiceCount > 0)
                        {
                            ok = true;
                            Program.InteractiveEditService(memoName, memoPath, memo);
                            break;
                        }
                    }
                    Console.WriteLine("ERROR! Invalid number");
                }
                if (ok)
                {
                    continue;
                }
                Console.Error.WriteLine("ERROR! Too many try");
                return;
            }
        }

        private static bool InteractiveConfirm(string msg, ref int index)
        {
            Console.WriteLine("0:(Cancel)");
            Console.WriteLine("1: {0}", msg);
            for (int i = 0; i < 10; i++)
            {
                Console.Write("command number ? ");
                Console.Out.Flush();
                string line = Console.ReadLine().Trim();
                if (Int32.TryParse(line, out index))
                {
                    if (index == 0 || index == 1)
                    {
                        return true;
                    }
                }
                else if ("cancel".StartsWith(line.ToLower()))
                {
                    index = 0;
                    return true;
                }
                Console.WriteLine("ERROR! Invalid number");
            }
            Console.Error.WriteLine("ERROR! Too many try");
            return false;
        }

        private static void InteractiveChangePassword(string memoPath, Idpwmemo.IDPWMemo memo)
        {
            string password = Program.ReadPassword();
            int index = 0;
            string msg = String.Format("Change to new password '{0}'", password);
            if (InteractiveConfirm(msg, ref index))
            {
                if (index == 1)
                {
                    memo.ChangePassword(password);
                    password = null;
                    Program.SaveMemo(memoPath, memo);
                    Console.WriteLine("Changed password and saved");
                    return;
                }
            }
            Console.WriteLine("Canceled to change password");
        }

        private static void InteractiveAddNewService(string memoName, string memoPath, Idpwmemo.IDPWMemo memo)
        {
            Console.Write("new service name ? ");
            Console.Out.Flush();
            string serviceName = Console.ReadLine();
            if (String.IsNullOrWhiteSpace(serviceName))
            {
                Console.WriteLine("ERROR! Invalid service name");
                Console.WriteLine("Canceled to add new service");
                return;
            }
            memo.AddNewService(serviceName);
            Program.InteractiveEditServiceValues(memoName, memoPath, memo, serviceName);
        }

        private static void InteractiveEditService(string memoName, string memoPath, Idpwmemo.IDPWMemo memo)
        {
            List<string> nameList = memo.GetServiceNames();
            int idWidth = Program.IntegerWidth(nameList.Count);
            Console.WriteLine();
            Console.WriteLine("[ {0} ]", memoName);
            Console.WriteLine("{0}:(CANCEL)", "0".PadLeft(idWidth));
            for (int i = 0; i < nameList.Count; i++)
            {
                string n = (i + 1).ToString().PadLeft(idWidth);
                Console.WriteLine("{0}: {1}", n, nameList[i]);
            }
            for (int i = 0; i < 10; i++)
            {
                Console.Write("service number ? ");
                Console.Out.Flush();
                string line = Console.ReadLine();
                int index = 0;
                if (Int32.TryParse(line, out index))
                {
                    if (index == 0)
                    {
                        Console.WriteLine("Canceled to edit service");
                        return;
                    }
                    if (index > 0 && index <= nameList.Count)
                    {
                        string serviceName = nameList[index - 1];
                        memo.SelectService(index - 1);
                        Program.InteractiveEditServiceValues(memoName, memoPath, memo, serviceName);
                        return;
                    }
                }
                else
                {
                    index = Program.FindNameIndex(nameList, line);
                    if (index >= 0)
                    {
                        string serviceName = nameList[index];
                        memo.SelectService(index);
                        Program.InteractiveEditServiceValues(memoName, memoPath, memo, serviceName);
                        return;
                    }
                }
                Console.WriteLine("ERROR! Invalid number");
            }
            Console.Error.WriteLine("ERROR! Too many try");
            Console.WriteLine("Canceled to edit service");
        }

        private static void InteractiveEditServiceValues(string memoName, string memoPath, Idpwmemo.IDPWMemo memo, string serviceName)
        {
            for (; ; )
            {
                Console.WriteLine();
                Console.WriteLine("[ {0} ] {1}", memoName, serviceName);
                Console.WriteLine("0:(Cancel)");
                if (memo.IsServiceEdited())
                {
                    Console.WriteLine("1: Save memo *");
                }
                else
                {
                    Console.WriteLine("1: Save memo");
                }
                Console.WriteLine("2: Add new detail");
                if (memo.GetValues().Count > 0)
                {
                    Console.WriteLine("3: Edit detail");
                }
                Console.WriteLine("4: Add new secret");
                if (memo.GetSecrets().Count > 0)
                {
                    Console.WriteLine("5: Edit secret");
                }
                bool ok = false;
                for (int i = 0; i < 10; i++)
                {
                    Console.Write("command number ? ");
                    Console.Out.Flush();
                    string line = Console.ReadLine().Trim();
                    int index = 0;
                    if (Int32.TryParse(line, out index))
                    {
                        switch (index)
                        {
                            case 0: // Cancel
                                return;
                            case 1: // Save memo
                                if (!Program.InteractiveSaveMemo(memoName, memoPath, memo, serviceName))
                                {
                                    return;
                                }
                                serviceName = memo.GetSelectedServiceName();
                                ok = true;
                                break;
                            case 2: // Add new detail
                                Program.InteractiveAddNewValue(memo.GetValues());
                                ok = true;
                                break;
                            case 3: // Edit detail
                                if (memo.GetValues().Count > 0)
                                {
                                    Program.InteractiveEditValue(memo.GetValues());
                                    ok = true;
                                }
                                break;
                            case 4: // Add new secret
                                Program.InteractiveAddNewValue(memo.GetSecrets());
                                ok = true;
                                break;
                            case 5: // Edit secret
                                if (memo.GetSecrets().Count > 0)
                                {
                                    Program.InteractiveEditValue(memo.GetSecrets());
                                    ok = true;
                                }
                                break;
                            default:
                                break;
                        }
                        if (ok)
                        {
                            break;
                        }
                    }
                    else if ("cancel".StartsWith(line.ToLower()))
                    {
                        return;
                    }
                    else if ("save memo".StartsWith(line.ToLower()))
                    {
                        if (!Program.InteractiveSaveMemo(memoName, memoPath, memo, serviceName))
                        {
                            return;
                        }
                        serviceName = memo.GetSelectedServiceName();
                        ok = true;
                        break;
                    }
                    Console.WriteLine("ERROR! Invalid number");
                }
                if (ok)
                {
                    continue;
                }
                Console.Error.WriteLine("ERROR! Too many try");
                return;
            }
        }

        private static void InteractiveEditValue(List<Idpwmemo.Value> values)
        {
            int idWidth = Program.IntegerWidth(values.Count);
            int width = 0;
            foreach (Idpwmemo.Value v in values)
            {
                width = Math.Max(width, v.TypeName.Length);
            }
            Console.WriteLine();
            Console.WriteLine("Select value");
            Console.WriteLine("{0}:(Cancel)", "0".PadLeft(idWidth));
            for (int i = 0; i < values.Count; i++)
            {
                Idpwmemo.Value v = values[i];
                Console.WriteLine("{0}: {1}: {2}", (i + 1).ToString().PadLeft(idWidth), v.TypeName.PadRight(width), v.Data);
            }
            Idpwmemo.Value selValue = null;
            bool ok = false;
            for (int i = 0; i < 10; i++)
            {
                Console.Write("value number ? ");
                Console.Out.Flush();
                string line = Console.ReadLine().Trim();
                int index = 0;
                if (Int32.TryParse(line, out index))
                {
                    if (index == 0)
                    {
                        Console.WriteLine("Canceled to edit value");
                        return;
                    }
                    else if (0 < index && index <= values.Count)
                    {
                        ok = true;
                        selValue = values[index - 1];
                        break;
                    }
                }
                else if ("cancel".StartsWith(line.ToLower()))
                {
                    Console.WriteLine("Canceled to edit value");
                    return;
                }
                Console.WriteLine("ERROR! Invalid number");
            }
            if (!ok)
            {
                Console.Error.WriteLine("ERROR! Too many try");
                Console.WriteLine("Canceled to edit value");
                return;
            }
            Console.WriteLine("Selected ( {0}: {1} )", selValue.TypeName, selValue.Data);
            Console.Write("New value ? ");
            Console.Out.Flush();
            string newValue = Console.ReadLine();
            if (String.IsNullOrWhiteSpace(newValue))
            {
                newValue = "";
            }
            selValue.Data = newValue;
            Console.WriteLine("Edited value ( {0}: {1} )", selValue.TypeName, selValue.Data);
        }

        private static void InteractiveAddNewValue(List<Idpwmemo.Value> values)
        {
            Console.WriteLine();
            Console.WriteLine("Select type");
            Console.WriteLine("0:(Cancel)");
            for (int i = 0; i < 8; i++)
            {
                string s = Idpwmemo.ValueTypeExtensions.TypeName((Idpwmemo.ValueType)(byte)i);
                Console.WriteLine("{0}: {1}", i + 1, s);
            }
            var valueType = Idpwmemo.ValueType.Id;
            bool ok = false;
            for (int i = 0; i < 10; i++)
            {
                Console.Write("type number ? ");
                Console.Out.Flush();
                string line = Console.ReadLine().Trim().ToLower();
                int index = 0;
                if (Int32.TryParse(line, out index))
                {
                    if (index == 0)
                    {
                        Console.WriteLine("Canceled to add new value");
                        return;
                    }
                    else if (0 < index && index <= 8)
                    {
                        ok = true;
                        valueType = (Idpwmemo.ValueType)(byte)(index - 1);
                        break;
                    }
                }
                else if ("cancel".StartsWith(line))
                {
                    Console.WriteLine("Canceled to add new value");
                    return;
                }
                else
                {
                    for (int x = 0; x < 8; x++)
                    {
                        Idpwmemo.ValueType vt = (Idpwmemo.ValueType)(byte)x;
                        string s = Idpwmemo.ValueTypeExtensions.TypeName(vt).ToLower();
                        if (s.EndsWith(line) || s.StartsWith(line))
                        {
                            valueType = vt;
                            ok = true;
                            break;
                        }
                    }
                    if (ok)
                    {
                        break;
                    }
                }
                Console.WriteLine("ERROR! Invalid number");
            }
            if (!ok)
            {
                Console.Error.WriteLine("ERROR! Too many try");
                Console.WriteLine("Canceled to add new value");
                return;
            }
            Console.WriteLine("Type: {0}", Idpwmemo.ValueTypeExtensions.TypeName(valueType));
            Console.Write("Value ? ");
            Console.Out.Flush();
            string valueData = Console.ReadLine();
            var newValue = new Idpwmemo.Value(valueType, valueData);
            values.Add(newValue);
            Console.WriteLine("Added new value ( {0}: {1} )", newValue.TypeName, newValue.Data);
        }

        private static void SaveMemo(string memoPath, Idpwmemo.IDPWMemo memo)
        {
            memo.ConvertV1ToV2();
            byte[] data = memo.Save();
            File.WriteAllBytes(memoPath, data);
        }

        private static bool InteractiveSaveMemo(string memoName, string memoPath, Idpwmemo.IDPWMemo memo, string serviceName)
        {
            if (memo.GetSelectedService().ValidState)
            {
                memo.UpdateSelectedService();
                Program.SaveMemo(memoPath, memo);
                Console.WriteLine("Saved");
                return true;
            }
            if (memo.ServiceCount == 1)
            {
                // Delete Memo
                string msg = String.Format("Delete memo {0} ?", memoName);
                int index = 0;
                if (!Program.InteractiveConfirm(msg, ref index))
                {
                    Console.WriteLine("Canceled to save");
                    return true;
                }
                if (index == 0)
                {
                    Console.WriteLine("Canceled to save");
                    return true;
                }
                if (File.Exists(memoPath))
                {
                    File.Delete(memoPath);
                }
                Console.WriteLine("Delete memo {0}", memoName);
            }
            else
            {
                // Delete Service
                string msg = String.Format("Delete service {0} from {1} ?", serviceName, memoName);
                int index = 0;
                if (!Program.InteractiveConfirm(msg, ref index))
                {
                    Console.WriteLine("Canceled to save");
                    return true;
                }
                if (index == 0)
                {
                    Console.WriteLine("Canceled to save");
                    return true;
                }
                memo.RemoveSelectedService();
                Program.SaveMemo(memoPath, memo);
                Console.WriteLine("Delete service {0} from {1}", serviceName, memoName);
            }
            return false;
        }

        private static string InteractiveSelectMemo(string dir)
        {
            List<Tuple<string, string>> list = Program.GetMemoFiles(dir);
            Console.WriteLine("0:(QUIT)");
            Console.WriteLine("1: New memo");
            if (list.Count > 0)
            {
                Console.WriteLine("2: Edit memo");
            }
            for (int i = 0; i < 10; i++)
            {
                Console.Write("command number ? ");
                Console.Out.Flush();
                string line = Console.ReadLine().Trim().ToLower();
                int index = 0;
                if (Int32.TryParse(line, out index))
                {
                    if (index == 0)
                    {
                        return null;
                    }
                    if (index == 1)
                    {
                        return Program.InteractiveMakeNewMemo(dir);
                    }
                    if (index == 2 && list.Count > 0)
                    {
                        return Program.InteractiveLoadMemo(dir, list);
                    }
                }
                else if ("quit".StartsWith(line))
                {
                    return null;
                }
                else if ("new memo".StartsWith(line))
                {
                    return Program.InteractiveMakeNewMemo(dir);
                }
                else if ("edit memo".StartsWith(line) && list.Count > 0)
                {
                    return Program.InteractiveLoadMemo(dir, list);
                }
                Console.WriteLine("ERROR! Invalid number");
            }
            Console.Error.WriteLine("ERROR! Too many try");
            return null;
        }

        private static string InteractiveMakeNewMemo(string dir)
        {
            Regex regex = Program.GetMemoNameRegex();
            for (int i = 0; i < 10; i++)
            {
                Console.Write("new memo name ? ");
                Console.Out.Flush();
                string memoName = Console.ReadLine().Trim();
                if (String.IsNullOrWhiteSpace(memoName) || !regex.IsMatch(memoName))
                {
                    Console.WriteLine("ERROR! Invalid memo name");
                    continue;
                }
                string p = Path.Combine(dir, memoName + ".memo");
                if (File.Exists(p))
                {
                    Console.WriteLine("ERROR! Already existed memo name");
                    continue;
                }
                return memoName;
            }
            Console.Error.WriteLine("ERROR! Too many try");
            return null;
        }

        private static string InteractiveLoadMemo(string dir, List<Tuple<string, string>> list)
        {
            Console.WriteLine();
            Console.WriteLine("{0}:(CANCEL)", "0".PadLeft(Program.IntegerWidth(list.Count)));
            Program.ShowMemoList(list);
            for (int i = 0; i < 10; i++)
            {
                Console.Write("memo number ? ");
                Console.Out.Flush();
                string line = Console.ReadLine();
                int index = 0;
                if (Int32.TryParse(line, out index))
                {
                    if (index == 0)
                    {
                        return null;
                    }
                    if (index > 0 && index <= list.Count)
                    {
                        return list[index - 1].Item2;
                    }
                }
                else
                {
                    foreach (Tuple<string, string> elem in list)
                    {
                        if (elem.Item2 == line)
                        {
                            return elem.Item2;
                        }
                    }
                }
                Console.WriteLine("ERROR! Invalid number");
            }
            Console.Error.WriteLine("ERROR! Too many try");
            return null;
        }
    }
}